/*Developed by josleugim@gmail.com*/
$.stellar();

$(function () {
	adjustSectionHeight();

	$(window).scroll(function() {
		var sVal = $(window).scrollTop();
		if (sVal > 425) {
			$('.dont-worry-content img').css('display','block');
		}
		else if (sVal <= 425) {
			$('.tuist-content img').css('display','block');
			$('.dont-worry-content img').css('display','none');
		}
		if (sVal > 1195) {
			$('.tuist-content img').css('display','none');
			$('.be-happy-content img').css('display','block');
		}
		else if (sVal <= 1195) {
			$('.be-happy-content img').css('display','none');
		}
		if (sVal > 1952) {
			$('.todo-content img').css('display','block');
			$('.dont-worry-content img').css('display','none');
			$('.be-happy-content img').css('display','none')
		}
		else if (sVal <= 1952) {
			$('.todo-content img').css('display','none');
		}
		if (sVal > 2720) {
			$('.book-content img').css('display','block');
			$('.dont-worry-content img').css('display','none');
			$('.be-happy-content img').css('display','none')
			$('.todo-content img').css('display','none');
		}
		else if (sVal <= 2720) {
			$('.book-content img').css('display','none');
		}
		if (sVal > 3600) {
			$('.contact-content img').css('display','block');
			$('.dont-worry-content img').css('display','none');
			$('.be-happy-content img').css('display','none')
			$('.todo-content img').css('display','none');
			$('.book-content img').css('display','none');
		}
		else if (sVal <= 3600) {
			$('.contact-content img').css('display','none');
		}
	});
});

function adjustSectionHeight(){
	var wHeight = $( window ).height();
	
	if(wHeight > 768){
		var diffHeight = wHeight - 768;
		$('#tuist').css('height', diffHeight + 768);
	}
};

function doMail() {
	location.href = "mailto:jorge@tuist.com.mx";
};